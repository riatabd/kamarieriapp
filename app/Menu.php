<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $table = 'menu';

    protected $fillable =  ['price','business_id','product_id'];

    public function product()
    {
        return $this->hasOne('App\Product', 'id', 'product_id');
    }

    public function business()
    {
        return $this->hasOne('App\Business', 'id', 'business_id');
    }
}
