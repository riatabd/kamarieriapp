<?php
/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});



/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //
    Route::resource('business','BusinessController');
    //Route::resource('business/getcategory','BusinessController@getCategories');
    //Route::resource('business/{id}/getproducts/{categoryid}','BusinessController@getProducts');
    Route::resource('category','CategoryController');
    Route::resource('products','ProductController');
    Route::resource('menu','MenuController');
    Route::resource('restapp','RestController');
    Route::resource('ingredients/{id}','RestController@getIngredients');

});
