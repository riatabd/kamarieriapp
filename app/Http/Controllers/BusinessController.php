<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Business;
use DB;

use App\Http\Requests;

class BusinessController extends Controller
{
    public function index(){

    	$business = Business::all();

    	return view('business.index', compact('business'));
    }

    public function create() {

    	return view('business.create');
    }

    public function store(Request $request) {

    	Business::create($request->all());

		return redirect()->route('business.index');

    }

    public function show($id) {
            return Business::findOrFail($id);     
        }

    public function getCategories($id) {
        $category = DB::select('SELECT category.id, category.name
                                FROM category, menu, products, business
                                WHERE menu.product_id = products.id
                                    AND products.category_id = category.id 
                                    AND menu.business_id = business.id
                                    AND business.id = '.$id.'
                                    GROUP BY category.name'
                                );
        return $category;
    }

    public function getProducts($id, $categoryid) {
        $products = DB::select("SELECT products.id, products.name
                                FROM category, menu, products, business
                                WHERE menu.product_id = products.id 
                                    AND products.category_id = category.id 
                                    AND menu.business_id = business.id
                                    AND products.category_id = $categoryid
                                    AND menu.business_id = $id
                                 GROUP BY products.name"
                                );
        return $products;
    }

}
