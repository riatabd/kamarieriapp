<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RestModel;
use App\Business;
use App\Menu;
use App\Product;
use DB;
use Response;
use App\Http\Requests;

class RestController extends Controller
{
    public function show($id) {

    		$menuproducts = DB::select("SELECT 
    										business.name as bizname, 
    										category.name as categoryname, 
    										products.name as productname,
                                            menu.product_id as productid,
                                            menu.price as productprice,
                                            menu.id as menuid
									FROM menu
										INNER JOIN products ON products.id = menu.product_id
										INNER JOIN category ON category.id = products.category_id
										INNER JOIN business ON business.id = menu.business_id
									WHERE menu.business_id = $id"
                                );

                    $rows = json_decode(json_encode($menuproducts), true);
                    
                    $res=array();

                    foreach($rows as $row=>$value)
                    {
                        $res[$value['bizname']][$value['categoryname']][$value['productid']][$value['productname']][] = $value['productprice'];
                    }
   
                    return Response::json($res, 200, array(), JSON_PRETTY_PRINT);
        }

        public function getIngredients($id) {
            $ingredients = DB::select("SELECT ingredients.name, ingredients.price, menu.product_id FROM ingredients, menu 
                                        WHERE menu.business_id = $id AND ingredients.menu_id = menu.id");

            return Response::json($ingredients, 200, array(), JSON_PRETTY_PRINT);
        }

}



