<?php
namespace App\Http\Controllers;
use App\Business;
use App\Menu;
use App\Product;
use App\Category;
use App\Ingredients;
use DB;
use Response;

use Illuminate\Http\Request;

use App\Http\Requests;

class MenuController extends Controller
{
    public function index(){

    	$menu = Menu::all();

    	return view('menu.index', compact('menu'));
    }

    public function create() {
    	$businesses = Business::lists('name','id');
    	$products = Product::lists('name','id');

    	return view('menu.create', compact('businesses', 'products'));
    }

    public function store(Request $request) {

    	Menu::create($request->all());
        
        /*
        $menu = new Menu();
        $menu->price = $request->price;
        $menu->business_id = $request->business_id;
        $menu->product_id = $request->product_id;

        $menu->save();

        $insertedmenuid = $menu->id;

        $ingredient = new Ingredients();

        $ingredient->menu_id = $insertedmenuid;
        $ingredient->name = $request->ingredient_name;
        $ingredient->price = $request->ingredient_price;

        $ingredient->save();
        */

		return redirect()->route('menu.create');

    }

    public function show($id) 
    	{
            
            $read = DB::table('menu')
                    ->join('products', 'products.id', '=', 'menu.product_id')
                    ->join('category', 'category.id', '=','products.category_id')
                    ->join('business', 'business.id', '=','menu.business_id')
                    ->select('category.name as categoryname','products.name as productname','menu.price as productprice')
                    ->where('menu.business_id', $id)->get();
           
            //return $read;    
            return Response::json($read, 200, array(), JSON_PRETTY_PRINT); 

        }
}
