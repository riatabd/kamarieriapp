<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;

use App\Http\Requests;

class CategoryController extends Controller
{
    public function index() {

    	$categories = Category::all();

    	return view('category.index', compact('categories'));
    }

    public function create() {
    	return view('category.create');
    }

    public function store(Request $request) {

    	Category::create($request->all());
		return view('category.create');

    }

    public function show($id) {
            return Category::findOrFail($id);    
    }

}
