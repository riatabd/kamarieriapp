<?php
namespace App\Http\Controllers;
use App\Product;
use App\Category;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
	public function index() {

		$products = Product::all();

		return view('products.index', compact('products'));
	}

    public function create() {

    	$category = Category::lists('name','id');
        
        return view('products.create', compact('category'));
    }

    public function store(Request $request) {
    	
    	Product::create($request->all());
		
		//return view('products.create');
		return redirect()->route('products.create');

    }

    public function view() {

    }
}
