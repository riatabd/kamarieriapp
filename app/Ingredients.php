<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ingredients extends Model
{
    protected $table = 'ingredients';
    protected $dates = ['created_at', 'updated_at'];

    public function menu()
	    {
			return $this->belongsTo('App\Menu');
		}
}
