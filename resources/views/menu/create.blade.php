<!DOCTYPE html>
<html>
<head>
	<title>Create Menu</title>
</head>
<body>

{!! Form::open(array('route' => 'menu.store', 'id' => 'form-with-validation', 'class' => 'form-horizontal')) !!}

<div class="form-group">
    {!! Form::label('price', 'Price', array('class'=>'col-md-2 control-label')) !!}
    <div class="col-sm-10">
        {!! Form::text('price', null, array('class'=>'form-control')) !!}
    </div>
</div>

<div class="form-group">
{!! Form::label('product_id', 'Product', array('class'=>'col-md-2 control-label')) !!}
    <div class="col-sm-10">
        {{ Form::select('product_id', $products) }}
    </div>
</div>

<div class="form-group">
{!! Form::label('business_id', 'Business', array('class'=>'col-md-2 control-label')) !!}
    <div class="col-sm-10">
		{{ Form::select('business_id', $businesses) }}
	</div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label">&nbsp;</label>
    <div class="col-sm-10">
      {!! Form::submit('Create', array('class' => 'btn btn-primary')) !!}
    </div>
</div>

{!! Form::close() !!}
</body>
</html>