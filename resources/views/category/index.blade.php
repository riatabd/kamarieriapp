<!DOCTYPE html>
<html>
<head>
	<title>List Categories</title>
</head>
<body>
	@foreach($categories as $category)
		{{ $category->id }} -
		{{ $category->name }} <br />
		<ul>
			@foreach ($category->products as $product)
			<li>{{ $product->name }}</li>
			@endforeach
		</ul>
		<hr />
	@endforeach
</body>
</html>