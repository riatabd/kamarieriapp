<!DOCTYPE html>
<html>
<head>
	<title>Create Product</title>
</head>
<body>

{!! Form::open(array('route' => 'products.store', 'id' => 'form-with-validation', 'class' => 'form-horizontal')) !!}

<div class="form-group">
    {!! Form::label('Name', 'name', array('class'=>'col-md-2 control-label')) !!}
    <div class="col-sm-10">
        {!! Form::text('name', null, array('class'=>'form-control')) !!}
    </div>
</div>
<div class="form-group">
    <div class="col-sm-10">
		{{ Form::select('category_id', $category) }}
	</div>
</div>

<div class="form-group">
    <label class="col-sm-2 control-label">&nbsp;</label>
    <div class="col-sm-10">
      {!! Form::submit('Create', array('class' => 'btn btn-primary')) !!}
    </div>
</div>

{!! Form::close() !!}
</body>
</html>